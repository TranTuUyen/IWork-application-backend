const passport = require('passport');
const LocalStategy = require('passport-local').Strategy;

const JwtStategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const bcrypt = require('bcrypt');

const userModel = require('../models/user');

let jwtOptions = require('./config.json');
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

passport.serializeUser(function(user, cb) {
    cb(null, user._id);
});

passport.deserializeUser(function(id, cb) {
    User.findById(id, function(err, user) {
        cb(err, user);
    });
});

passport.use(new JwtStategy(jwtOptions, (jwtPayload, done) => {
    console.log("jwt payload: " + JSON.stringify(jwtPayload));
    return done(null, jwtPayload);
}));

passport.use(new LocalStategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false
}, (username, password, done) => {
    userModel.findOne({email: username}, (err, user) => {
        if (err) return done(err);
        else if (user && bcrypt.compareSync(password.concat(username), user.password)) {
            return done(null, user);
        } else return done(null, false);
    });
}));

module.exports = passport;