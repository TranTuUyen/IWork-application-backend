var mongoose = require('mongoose');

var connectDatabase = () => {
	mongoose.connect('mongodb://localhost/iworkdatabase', function(error) {
		if(error) {
			console.log("Connect to database fail!")
		}
		else {
			console.log("Connect to database successfull!")
		}
	});
}

module.exports = {connectDatabase}