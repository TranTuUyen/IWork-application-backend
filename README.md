| Language | Framework | Platform | Author |
| -------- | -------- |--------|--------|
| Nodejs | Express | Azure Web App, Virtual Machine| |


# Nodejs Express web application

Sample Nodejs Express web application built using Visual Studio 2017.

## License:

See [LICENSE](LICENSE).

## Contributing

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.##

Run: npm install then
node app

##API:
###Sign up: 
path: ```/auth/sign-up```
payload: 
- ```email``` (required) user email address
-```password`` (required) user password
response
```json
{
    "user": {
      "_id": string,
      "email": string, 
    },
    "token": string 
}
```

###Login
payload: 
- ```email``` (required) user email address
-```password`` (required) user password

response:

-```token```: string jwt token string

##Team
###Add new
- path: ```/team``` 
- method: ```POST``` 
- payload:
```json
{
  "name": "string (required)",
  "profilePicture": "string (required)",
  "members": "[] array (optional)"
}
```
- response: created
```json
{
    "members": [
        {
            "_id": "5b1f42aeed66cc2a2478dfb9",
            "member": "5b1e9e353c6e0f1a2bfb0275",
            "permission": "Admin"
        }
    ],
    "_id": "5b1f3ee4af6593297ba13b82",
    "name": "team 1",
    "profilePicture": "/picture1.jpg",
    "__v": 0
}
```

###Update info
- path ```/team/:id``` id of team for update info
- method: ```PUT```
- payload:
```json
{
  "name": "string (required)",
  "profilePicture": "string (required)",
}
```
- response: team updated
```json
{
    "members": [
        {
            "_id": "5b1f42aeed66cc2a2478dfb9",
            "member": "5b1e9e353c6e0f1a2bfb0275",
            "permission": "Admin"
        }
    ],
    "_id": "5b1f3ee4af6593297ba13b82",
    "name": "team 1",
    "profilePicture": "/picture1.jpg",
    "__v": 0
}
```
##Update member.

- path ```/team/:id/:action/:memberId```
    + id is team id for update 
    + action is action has 3 status 
        - ```change-member-permission``` for change member permssion
        - ```add-member``` for add new member to team
        - ```remove-member``` for remove old member from team
    + memberId: member id for update
- method: ```PUT```
- payload
```json
{
  "permission": "string (Admin or Member)"
}
``` 
- response: team updated
```json
{
    "members": [
        {
            "_id": "5b1f42aeed66cc2a2478dfb9",
            "member": "5b1e9e353c6e0f1a2bfb0275",
            "permission": "Admin"
        }
    ],
    "_id": "5b1f3ee4af6593297ba13b82",
    "name": "team 1",
    "profilePicture": "/picture1.jpg",
    "__v": 0
}
```
