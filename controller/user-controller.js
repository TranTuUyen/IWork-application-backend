const userModel = require('../models/user');

module.exports = {
  findById: (id) => {
    return userModel.findOneAsync({_id: id});
  },
  findByEmail: (email) => {
    return userModel.findAsync({email: email});
  }
}
