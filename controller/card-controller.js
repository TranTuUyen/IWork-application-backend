const cardModel = require('../models/card');

module.exports = {
  findById: (id) => {
    return cardModel.findOneAsync({_id: id});
  },
  findByUserId: (userId) => {
    return cardModel.findAsync({members: userId});
  },
  findByTeamId: (teamId) => {
    return cardModel.findAsync({team: teamId});
  },
  insert: (card) => {
    let newCard = new cardModel(card);
    return newCard.save();
  },
  update: (actor, card) => {
    let query = {_id: card._id, members: actor._id};
    let cardUpdate = {};
    if (card.content)  cardUpdate.content = card.content;
    if (card.startDate)  cardUpdate.startDate = card.startDate;
    if (card.dueDate)  cardUpdate.dueDate = card.dueDate;
    if (card.status)  cardUpdate.status = card.status;
    if (card.index)  cardUpdate.index = card.index;
    console.log("Query update: " + JSON.stringify(query));
    return cardModel.findOneAndUpdate(query, {$set:  cardUpdate}, {'new': true})
  },
  updateMember: (card, action, member) => {
    let query = {_id: card._id};
    let cardUpdate = {};
    if (action == 'add-member') {
      cardUpdate.$push = {members: member._id};
    }
    if (action == 'remove-member') {
      cardUpdate.$pull = {members: member._id};
    }
    return cardModel.findOneAndUpdate({_id: card._id}, cardUpdate, {'new': true});
  },
  delete: (card) => {
    let query = {_id: card._id};
    return cardModel.remove(query);
  }
};