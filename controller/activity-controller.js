const activityModel = require('../models/activity');
const cardController = require('../controller/card-controller');
const userController = require('../controller/user-controller');

module.exports = {
  findByTeamId: (teamId) => {
    return activityModel.findAsync({team: teamId});
  },
  insert: (activity) => {
    let newActivity = new activityModel(activity);
    return newActivity.save();
  },
  newActivity: (actorId, teamID, card, targetId, action) => {
    var newCardActivity = {} ;
    newCardActivity.actor = actorId;
    newCardActivity.target = targetId;
    newCardActivity.cardContent = card.content;
    newCardActivity.cardStatus = card.status;
    newCardActivity.team = teamID ;
    newCardActivity.action = action;
    newCardActivity.timeStamp = Date.now();
    return newCardActivity;
  },
  delete: (act) => {
    let query = {_id: act._id};
    return activityModel.remove(query);
  },
  getUser:(id) => {
  //Get actor and target
    return new Promise((resolve, reject) => {
      userController.findById(id)
      .then(user => {
        resolve(user);
      })
      .catch(function () {
          console.log('No actor');
        })
      })
  }
}
