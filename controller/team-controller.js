const teamModel = require('../models/team');

module.exports = {
  findById: id => {
    return teamModel.findOneAsync({_id: id});
  },
  findByUserId: (userId) => {
    return teamModel.findAsync({'members.member': userId});
  },
  insert: (team) => {
    let newTeam = new teamModel(team);
    return newTeam.saveAsync();
  },
  update: (admin, team) => {
    let json = {};
    if (team.name) json.name = team.name;
    if (team.profilePicture) json.profilePicture = team.profilePicture;
    return teamModel.update({
      _id: team._id,
      members: {$elemMatch: {member: admin._id, permission: 'Admin'}}
    }, {$set: json})
  },
  updateMember: (admin, team, action, member) => {
    // If action us change permission user
    let query = {_id: team._id, members: {$elemMatch: {member: admin._id, permission: 'Admin'}}};
    if (action == 'update-member-permission') {
      return new Promise((resolve, reject) => {
        teamModel.findOneAsync(query)
          .then(result => {
            if (!result) reject('Not found team');
            let index = result.members.findIndex(elem => {
              return elem.member == member._id;
            });
            if (index != -1) {
              result.members[index].permission = member.permission;
              teamModel.updateAsync({_id: team._id}, result);
              resolve(result);
            } else {
              reject('Not found');
            }
          })
          .catch(err => reject('Not found'));
      });
    } else {
      let jsonUpdate = {};
      if (action == 'add-member') jsonUpdate.$push = {members: {member: member._id, permission: member.permission}};
      if (action == 'remove-member') jsonUpdate.$pull = {members: {member: member._id, permission: member.permission}};
      return teamModel.findOneAndUpdateAsync(query, jsonUpdate, {"new": true});
    }
  },
  delete: (admin, team) => {
    let query = {_id: team._id, members: {$elemMatch: {member: admin._id, permission: 'Admin'}}};
    return teamModel.remove(query);
  }
}