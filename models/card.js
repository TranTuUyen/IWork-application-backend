const Promise 	= require('bluebird');
const mongoose 	= Promise.promisifyAll(require('mongoose'));
var TaskSchema = mongoose.Schema({
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Teams'
  },
  content: String,
  startDate: Date,
  dueDate: Date,
  status: String,
  index: Number,
  members: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users'
  }]
});

module.exports = mongoose.model('Cards', TaskSchema);