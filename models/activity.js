const Promise 	= require('bluebird');
const mongoose 	= Promise.promisifyAll(require('mongoose'));
// var mongoose = require('mongoose');

var ActivitySchema = mongoose.Schema({
  team: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Teams'
  },
  actor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users'
  },
  target: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users'
  },
  action: String,
  cardContent: String,
  cardStatus: String,
  timeStamp: Date
})

module.exports = mongoose.model('Activities', ActivitySchema)