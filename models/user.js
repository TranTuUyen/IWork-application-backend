var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: String
});

module.exports =  mongoose.model('Users', UserSchema)