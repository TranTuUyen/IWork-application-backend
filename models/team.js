const Promise 	= require('bluebird');
const mongoose 	= Promise.promisifyAll(require('mongoose'));

var TeamSchema = mongoose.Schema({
  name: String,
  profilePicture: String,
  members: [{
    member: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Users'
    },
    permission: {
      type: String,
      enum: ['Admin', 'Member']
    }
  }]
});

module.exports =  mongoose.model('Teams', TeamSchema)