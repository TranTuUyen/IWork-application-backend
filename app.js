﻿'use strict';


var debug = require('debug');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var cors = require('cors');

// Declare other Router
var routes = require('./routes/index');
var users = require('./routes/user');
var teams = require('./routes/team');
var activities = require('./routes/activity');
var cards = require('./routes/card').router;
var authRoute = require('./routes/auth');

// Use database
var database = require('./database/database');
//Passport Jwt
// const passport = require('./authentication/passport');


var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
let SCOPES = ['https://mail.google.com/',
  'https://www.googleapis.com/auth/gmail.modify',
  'https://www.googleapis.com/auth/gmail.compose',
  'https://www.googleapis.com/auth/gmail.send',
  'https://www.googleapis.com/auth/userinfo.profile']

var app = express();

// Declare Model
var Team = require('./models/team');
var Card = require('./models/card');

//Realtime
var http = require('http').Server(app);
var io = require('socket.io').listen(http);
require('./routes/card').io(io);
// Connect to database
database.connectDatabase();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Run server in localhost
app.use(cors())
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, api_key, Accept, Authorization');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  next();
});


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//   secret: 'work hard',
//   resave: true,
//   saveUninitialized: false
// }));

const passport = require('./authentication/passport');


app.use(passport.initialize());
app.use(passport.session());


//Use other Router
app.use('/', routes);
app.use('/user',  passport.authenticate('jwt', {session: false}), users);
app.use('/team', passport.authenticate('jwt', {session: false}), teams);
app.use('/card', passport.authenticate('jwt', {session: false}), cards);
app.use('/activity', passport.authenticate('jwt', {session: false}), activities);

app.use('/auth', authRoute);
const profile = require('./routes/profile');
app.use('/protected', profile);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.set('port', process.env.PORT || 3000);

var server = http.listen(3000, function () {
  debug('Express server listening on port ' + server.address().port);
});

