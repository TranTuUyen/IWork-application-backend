'use strict';

const jwt = require('jsonwebtoken');
const express = require('express');
let route = express.Router();
const jwtConfigs = require('../authentication/config');
const passport = require('../authentication/passport');
const userModel = require('../models/user');

const bcrypt = require('bcrypt');


route.post('/login', passport.authenticate('local'), (req, res) => {
    var secretOrKey = jwtConfigs.secretOrKey;
    var token = jwt.sign({email: req.user.email, _id: req.user._id}, secretOrKey, {
        expiresIn: 631139040,
        issuer: "account.localhost.com",
        audience: "localhost",
    });
    res.send({user: req.user, token: token});
});

route.post('/sign-up', (req, res, next) => {
    console.log("Req body: " + JSON.stringify(req.body));
    let password = req.body.password.concat(req.body.email);
    let user = userModel({email: req.body.email, password: bcrypt.hashSync(password, 10)});
    user.save(err => {
        if (err) {
            res.status(401);
            res.send({
                message: 'Sign up error'
            });
            res.end();
        } else {
            var secretOrKey = jwtConfigs.secretOrKey;
            var token = jwt.sign({email: user.email,_id: user._id}, secretOrKey, {
                expiresIn: 631139040,
                issuer: "account.localhost.com",
                audience: "localhost",
            });
            res.send({user: {_id: user._id, email: user.email}, token: token});
        }
    })
});

module.exports = route;
