'use strict';

var express = require('express');
var router = express.Router();

var Team = require('../models/team');
var Card = require('../models/card');


const teamController = require('../controller/team-controller');
/*Get team listing*/

router.get("/joined", function (req, res) {
  teamController.findByUserId(req.user._id)
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.status(500);
      res.send(err);
    })
})
/* GET cards listing by io */
router.get('/all-cards/:id', function (req, res) {
  var cardsArr = [];
  Team.findById(req.params.id, function (err, collection) {
    var len = collection.cards.length;
    for (var i = 0; i < len; i++) {
      Card.findById(collection.cards[i], function (err, result) {
        cardsArr.push(result);
        if (cardsArr.length === len) {
          res.send(cardsArr);
        }
      })
    }
  });
});

/*Create new team*/
router.post('/', function (req, res) {
  let newTeam = req.body;
  if (newTeam.members) {
    newTeam.push({member: req.user._id, permission: 'Admin'});
  } else {
    newTeam.members = [{member: req.user._id, permission: 'Admin'}];
  }
  teamController.insert(req.body)
    .then(result => {
      res.send(result);
    })
    .catch(error => {
      res.status(500);
      res.send(error);
    });
});

// Update team
router.put('/:id', function (req, res) {
  let teamUpdate = req.body;
  teamUpdate._id = req.params.id;
  teamController.update({_id: req.user._id}, teamUpdate)
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.status(500);
      res.send(err);
    })
});

router.put('/:id/:action/:memberId', function (req, res) {
  console.log(req.body);
  let admin = {_id: req.user._id};
  let team = {_id: req.params.id};
  let action = req.params.action;
  let member = {_id: req.params.memberId, permission: req.body.permission};
  teamController.updateMember(admin, team, action, member)
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.status(500);
      res.send(err)
    });
});

/*Delete team*/
router.delete('/:id', function (req, res) {
  let admin = req.user;
  let team = {_id: req.params.id};
  teamController.delete(admin, team)
    .then(result => res.send(result))
    .catch(err => {
      res.status(500);
      res.send(err);
    })
});

module.exports = router;