﻿'use strict';

var express = require('express');
var router = express.Router();
var User = require('../models/user');
const userController = require('../controller/user-controller');


/* GET users listing. */
router.get('/', function (req, res) {
  User.find(function (err, results) {
    console.log(results);
    res.send(results);
  })
});

// Get user by id

router.get('/:id' ,function (req, res) {
  userController.findById(req.params.id)
    .then(result => res.send(result))
.catch(err => {
    res.status(500);
  res.send({
    message: 'Error get data'
  });
});
});

// get user by email
router.get('/get-user-by-email/:email' ,function (req, res) {
  userController.findByEmail(req.params.email)
    .then(result => res.send(result))
.catch(err => {
    res.status(500);
  res.send({
    message: 'Error get data'
  });
});
});


router.get('/signup', function (req, res) {
  res.render('signup');
});

router.post('/signup', function (req, res) {
  var newUser = new User(req.body);
  newUser.save(function (err, result){
    res.send(result);
    console.log(newUser.email + 'saved to database');
  });
});

function checkSignIn(req, res, next) {
  if (req.session.user) {
    next();     //If session exists, proceed to page
  } else {
    var err = new Error("Not logged in!");
    console.log(req.session.user);
    next(err);  //Error, trying to access unauthorized page!
  }
}

router.get('/protected_page', checkSignIn, function (req, res) {
  res.render('protected_page', {email: req.session.user.email})
});

router.get('/login', function (req, res) {
  res.render('login');
});

router.post('/login', function (req, res) {
  if (!req.body.email || !req.body.password) {
    res.render('login', {message: "Please enter both io and password"});
  } else {
    console.log(req.body.email + " " + req.body.password);
    User.find({email: req.body.email, password: req.body.password}, function (err, users) {
      console.log(users);
      if (users && users.length > 0) {
        res.send(users[0]);
      }
      else {
        res.send(null);
      }
    });
  }
});

router.get('/logout', function (req, res) {
  req.session.destroy(function () {
    console.log("user logged out.")
  });
  res.redirect('/users/login');
});

router.use('/protected_page', function (err, req, res, next) {
  console.log(err);
  res.redirect('/users/login');
});

module.exports = router;
