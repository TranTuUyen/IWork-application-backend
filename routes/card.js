'use strict';

var express = require('express');
var router = express.Router();
const cardController = require('../controller/card-controller');
const avtivityController = require('../controller/activity-controller')
const passport = require('../authentication/passport');


var clients = 0;
exports.io = (io) => {
  io.on('connection', function (socket) {
    clients++;
    io.sockets.emit('broadcast', {description: clients + ' clients connected!'});
    console.log("Connected: "+socket.id);
    // console.log(socket);

    //Danh sach room dang co
    console.log(socket.adapter.rooms);
    socket.on("join-team-room", function(roomId) {
      socket.join(roomId);
    })
    /*Get Card listing*/
    router.get('/own' ,function (req, res) {
      cardController.findByUserId(req.user._id)
        .then(result => res.send(result))
        .catch(err => {
          res.status(500);
          res.send({
            message: 'Error get data'
          });
        });
    });

    // Get card by team
    router.get('/:id' ,function (req, res) {
      cardController.findByTeamId(req.params.id)
        .then(result => res.send(result))
      .catch(err => {
          res.status(500);
        res.send({
          message: 'Error get data'
        });
      });
    });

    // Get card by id
    router.get('/card-info/:id' ,function (req, res) {
      cardController.findById(req.params.id)
        .then(result => res.send(result))
      .catch(err => {
          res.status(500);
        res.send({
          message: 'Error get data'
        });
      });
    });

    /* Create new Card */
    router.post('/', function (req, res) {
      let newCard = req.body;
      if (!newCard.members) {
        newCard.members = [];
      }
      newCard.members.push(req.user._id);
      cardController.insert(newCard)
        .then(result => {
          io.sockets.to(newCard.team).emit('new-card', result);
          var newCardActivity = avtivityController.newActivity(req.user._id, req.body.team, newCard, null, "create-card");

          avtivityController.insert(newCardActivity)
            .then (newActivity => {
              getInfoAnActivity(newActivity)
              .then(function(activity){
                console.log(activity);
                io.sockets.to(newCard.team).emit('new-activity', activity);
                // console.log(newCardActivity);
              })
            })
        })
        .catch(err => {
          res.status(500);
          res.send(err);
        })
    });

    /*Update Card*/
    router.put('/:id', function (req, res) {
      let actor = req.user;
      let card = req.body;
      card._id = req.params.id;
      var newCardActivity;
      cardController.findById(card._id)
        .then(function(cardInfo){
          newCardActivity = avtivityController.newActivity(actor._id, card.team, cardInfo, null, "update-card");
        })
        .then(function() {
          cardController.update(actor, card)
            .then(result => {
            // console.log(result);
              io.sockets.emit('update-card', result);
            // res.send(result)

              avtivityController.insert(newCardActivity)
                .then (newActivity => {
                  getInfoAnActivity(newActivity)
                  .then(function(activity){
                    console.log(activity);
                    io.sockets.to(req.body.team).emit('new-activity', activity);
                  })
                })
                .catch(err => {
                    res.status(500);
                  res.send(err);
                })
              })
            .catch(err => res.send(err));
        })

    });



// Update member card
    router.put('/:id/:action/:memberId', function (req, res) {
      let actor = req.user;
      let card = {_id: req.params.id};
      let action = req.params.action;
      let member = {_id: req.params.memberId};
      // console.log(req.body);
      var newCardActivity;
      cardController.findById(card._id)
        .then(function(cardInfo){
          newCardActivity = avtivityController.newActivity(actor._id, req.body._id, cardInfo, member._id, action);
        })
        .then(function() {
          cardController.updateMember(card, action, member)
            .then(result => {
              io.sockets.emit('update-member-card', member);
              res.send(member);

              avtivityController.insert(newCardActivity)
                .then (newActivity => {
                  getInfoAnActivity(newActivity)
                  .then(function(activity){
                    console.log(activity);
                    io.sockets.to(activity.team).emit('new-activity', activity);
                  })
                })
            })
            .catch(err => res.send(err));
        })


    });

    /*Delete Card*/
    router.delete('/:id', function (req, res) {
      // console.log(req);
      let actor = req.user;
      let card = {_id: req.params.id};
      var newCardActivity;
      cardController.findById(card._id)
        .then(function(cardInfo){
          newCardActivity = avtivityController.newActivity(req.user._id, req.query._id, cardInfo, null, "delete-card");
        })
        .then(function() {
          avtivityController.insert(newCardActivity)
            .then (newActivity => {
              getInfoAnActivity(newActivity)
                .then(function(activity){
                  console.log(activity);
                  io.sockets.to(activity.team).emit('new-activity', activity);
                })
                .then(function() {
                  cardController.delete(card)
                    .then(result => {
                    io.sockets.emit('delete-card', card);
                  res.send(result)
                })
                .catch(err => res.send(err));
                })
            })
        })
    });
  });
};


function getInfoAnActivity(data) {
  let act = data;
  return new Promise(function(resolve, reject) {
    Promise.all([avtivityController.getUser(data.actor), avtivityController.getUser(data.target)])
      .then(function (result) {
        act.actor = result[0];
        act.target = result[1];
      })
      .then(function () {
        resolve(act);
      })
      .catch(function () {
        console.log("Sory")
      })
  })
}
exports.router = router;