'use strict';

var express = require('express');
var router = express.Router();
var Activity = require('../models/activity');
const activityController = require('../controller/activity-controller');
const cardController = require('../controller/card-controller');
const userController = require('../controller/user-controller');

// Get activities by team
router.get('/:id' ,function (req, res) {
  activityController.findByTeamId(req.params.id)
    .then(result => {
      // console.log(result.length);
        getDataInfo(result)
        .then(function(activities) {
          console.log("hahahaa")
          res.send(activities);
        })
    })
});

// Delete activity
router.delete('/:id', function (req, res) {
  let act = {_id: req.params.id};
  activityController.delete(act)
    .then(result => res.send(result))
    .catch(err => {
        res.status(500);
      res.send(err);
    })
});


function getDataInfo(data) {
  return new Promise(function(resolve, reject) {
    var activities = new Array();
    for (let i = 0; i < data.length; i++) {
      let act = data[i];
      Promise.all([activityController.getUser(data[i].actor), activityController.getUser(data[i].target)])
        .then(function (result) {
          console.log(1);

          act.actor = result[0];
          act.target = result[1];
        })
        .then(function () {
          activities.push(act);
          console.log(2);

        })
        .then(function () {
          console.log(3);

          if (i == data.length - 1) {
            resolve(activities);
          }
        })
        .catch(function () {
          console.log("Sory")
        })
    }
  })
}


module.exports = router;