'use strict';

var express = require('express');
var router = express.Router();
const passport = require('../authentication/passport');

router.get('/profile',
    passport.authenticate('jwt', {session: false}),
    function (req, res) {
        res.send(req.user);
    });
module.exports = router;